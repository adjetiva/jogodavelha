function Game() {
	this.board = [0,0,0,0,0,0,0,0,0];

	this.EMPTY = 0;
	this.X = 1;
	this.O = 2;

	this.currentPlayer = this.X;

	this.isBoardEmpty = function() {
		var empty = true;

		for (var i = 0; i < this.board.length; i++) {
			if (this.board[i] != this.EMPTY) {
				empty = false;
			}
		}

		return empty;
	}

	this.move = function(field) {
		if (this.board[field] != this.EMPTY) {
			return false;
		}

		this.board[field] = this.currentPlayer;

		if (this.currentPlayer == this.X) {
			this.currentPlayer = this.O;
		} else {
			this.currentPlayer = this.X;
		}

		return true;
	}
}