onload = function(){
	
	var matrix = document.getElementById("ticTacToe"),
		numbercolumns = 3,
		numberLines = 3,
		lineWidth = 2;
	
	// Line Width in Percentege
	lineWidthPercentage = Math.ceil(Math.min(matrix.width, matrix.height) / 100 * lineWidth);
	if (lineWidthPercentage % 2 != 0)
		lineWidthPercentage += 1;
	
	// Canvas Patterns 
	canvasContext = matrix.getContext("2d");
	canvasContext.lineCap = "round";
	canvasContext.lineWidth = lineWidthPercentage;
	
	drawBoard(matrix, numbercolumns, numberLines, lineWidthPercentage);
	clickElement(matrix, numbercolumns, numberLines, lineWidthPercentage);
	
}

function drawBoard( matrix, numbercolumns, numberLines, lineWidthPercentage ){
	
	for (var i = 1; i <= numbercolumns -1; i++) {
		var x = matrix.width / numbercolumns;
		var currentLine = Math.round(i * x);
		
		canvasContext.moveTo(currentLine, 0 + lineWidthPercentage / 2);
		canvasContext.lineTo(currentLine, matrix.height - lineWidthPercentage / 2);
	};

	for (var i = 1; i <= numberLines -1; i++) {
		var y = matrix.height / numberLines;
		var currentLine = Math.round(i * y);
		
		canvasContext.moveTo(0 + lineWidthPercentage / 2, currentLine);
		canvasContext.lineTo(matrix.width - lineWidthPercentage / 2, currentLine);
	};
	
	canvasContext.stroke();
	
}

function clickElement( matrix, numbercolumns, numberLines ){
	var currentClick = 0;

	matrix.onclick=function(){
		var xClickPercent   = event.layerX * 100 / matrix.width,
			yClickPercent   = event.layerY * 100 / matrix.height,
			columnClick     = Math.floor((numbercolumns / 100) * xClickPercent),
			lineClick       = Math.floor((numberLines / 100) * yClickPercent),
			lacunaWidth     = matrix.width / numbercolumns,
			lacunaHeight    = matrix.height / numberLines,
			leftPosition    = columnClick * lacunaWidth,
			topPosition     = lineClick * lacunaHeight,
			rightPosition   = leftPosition + lacunaWidth,
			bottomPosition  = topPosition + lacunaHeight,
			xCenter         = leftPosition + lacunaWidth / 2,
			yCenter         = topPosition + lacunaHeight / 2,		
			radius          = (Math.min(lacunaWidth, lacunaHeight) / 100 * 70) / 2 - (lineWidthPercentage);
			
		++currentClick;
			
		if (currentClick % 2 == 0) {
			drawCirc( xCenter, yCenter, radius );
		} else {
			drawX( xCenter, yCenter, radius );
		}
	}
	
}

function drawCirc( xCenter, yCenter, radius ){
	
	canvasContext.beginPath();
	canvasContext.arc(xCenter, yCenter, radius, 0, 2*Math.PI);
	canvasContext.strokeStyle = "green";
	canvasContext.stroke();
	
}

function drawX( xCenter, yCenter, radius ){
	
	var realLeftPosition = xCenter - radius,
		realTopPosition = yCenter - radius,
		realRightPosition = xCenter + radius,
		realBottomPosition = yCenter + radius;
	
	canvasContext.beginPath();
	
	canvasContext.moveTo(realLeftPosition, realTopPosition);
	canvasContext.lineTo(realRightPosition, realBottomPosition);
	
	canvasContext.moveTo(realRightPosition, realTopPosition);
	canvasContext.lineTo(realLeftPosition, realBottomPosition);
	canvasContext.strokeStyle = "red";
	canvasContext.stroke();
	
}