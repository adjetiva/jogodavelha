
describe("Game", function() {
	var game;

	beforeEach(function() {
		game = new Game();
	});

	describe("when has been started and human goes first", function() {
		it("should have empty board", function() {
			expect(game.isBoardEmpty()).toBeTruthy();
		}); 

		it("should start with X player", function() {
			expect(game.currentPlayer).toBe(game.X);
		}); 

		it("should move the board and board should not be empty", function() {
			game.move(1);
			expect(game.isBoardEmpty()).toBeFalsy();
		})
	});

	describe("when is the O player turn", function() {
		it("should not allow to move the same field", function() {
			game.move(1);
			expect(game.move(1)).toBeFalsy();
		});

		it("should move the field with the correct player", function() {
			game.move(0);
			expect(game.currentPlayer).toBe(game.O);
			game.move(2);
			expect(game.currentPlayer).toBe(game.X);
		});
	});
});